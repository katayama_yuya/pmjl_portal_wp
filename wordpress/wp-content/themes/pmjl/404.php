<?php
get_header();
?>

  <body <?php body_class(); ?>>
  <div class="container">
    <header class="header">
      <div class="inner header__inner">
        <h1 class="header__logo">
          <a href="./">
            <img src="<?php echoAssets('img'); ?>/common/logo.svg" alt="JAPAN LEAGUE PUBG MOBILE SEASON 0">
          </a>
        </h1>
        <nav>
          <ul class="header__list">
            <li class="header__item">
              <a href="./#program" class="header__link">
                PROGRAM
              </a>
            </li>
            <li class="header__item">
              <a href="./#schedule" class="header__link">
                SCHEDULE
              </a>
            </li>
            <li class="header__item">
              <a href="./#archives" class="header__link disable">
                ARCHIVES
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </header>


    <div class="content">

      <section class="head">
        <p class="head__logo">
          <picture>
            <source media="(max-width : 765px)" srcset="<?php echoAssets('img'); ?>/common/logo.svg">
            <img class="head__img" src="<?php echoAssets('img'); ?>/common/head-logo.png" alt="JAPAN LEAGUE PUBG MOBILE SEASON0">
          </picture>
        </p>
      </section>

      <div class="layout-simple  wow fadeIn">
        <h1 class="ttl">
          <span class="ttl__en">404</span>
          <span class="ttl__ja">PAGE NOT FOUND</span>
        </h1>
      </div>
    </div>



    <?php
      get_footer();
    ?>

  </div>
  </body>
  </html>
