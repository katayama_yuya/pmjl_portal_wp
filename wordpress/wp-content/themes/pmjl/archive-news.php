<?php
get_header();

$news_q = new WP_Query(
  array(
    'post_type'=>'news',
    'post_status' => 'publish',
  )
);
?>

<body <?php body_class(); ?>>
<div class="container">
  <?php
    include locate_template( 'template-parts/common/header_menu.php' );
    ?>


  <div class="content">

    <section class="head">
      <p class="head__logo">
        <picture>
          <source media="(max-width : 765px)" srcset="<?php echoAssets('img'); ?>/common/logo.svg">
          <img class="head__img" src="<?php echoAssets('img'); ?>/common/head-logo.png" alt="JAPAN LEAGUE PUBG MOBILE SEASON0">
        </picture>
      </p>
    </section>

    <div class="news wow fadeIn">

      <h1 class="ttl">
        <span class="ttl__en">NEWS</span>
        <span class="ttl__ja sp-hide">ニュース</span>
      </h1>


      <section class="news-list">
        <ul class="news-list__list">

          <?php if ($news_q->have_posts()) : ?>
              <?php while ($news_q->have_posts()) : $news_q->the_post(); ?>
                <li class="news-list__item">
                  <a class="news-list__link" href="<?php the_permalink(); ?>">
                    <span class="news-list__date"><?php the_time('Y.m.d'); ?></span>
                    <span class="news-list__ttl"><?php the_title() ?></span>
                  </a>
                </li>
              <?php endwhile; ?>
            <?php endif; ?>
        </ul>


      </section>


    </div>

  </div>

  <?php
  get_footer();
  ?>

</div>
</body>
</html>
