<?php
get_header();
$contact_pid = get_page_by_path('contact_data', 'OBJECT', 'editable_contents')->ID;
$league = get_field('for_league', $contact_pid );
$teamowner = get_field('for_team_owner', $contact_pid );
?>

  <body <?php body_class(); ?>>
  <div class="container">
    <?php
      include locate_template( 'template-parts/common/header_menu.php' );
      ?>

    <div class="content">

      <section class="head">
        <p class="head__logo">
          <picture>
            <source media="(max-width : 765px)" srcset="<?php echoAssets('img'); ?>/common/logo.svg">
            <img class="head__img" src="<?php echoAssets('img'); ?>/common/head-logo.png" alt="JAPAN LEAGUE PUBG MOBILE SEASON0">
          </picture>
        </p>
      </section>

      <div class="layout-simple  wow fadeIn">
        <h1 class="ttl">
          <span class="ttl__en">CONTACT</span>
          <span class="ttl__ja">お問い合わせ</span>
        </h1>
        <section class="contact">
          <p class="contact__description">
            <?php echo $league['text']; ?>
          </p>

          <div class="contact__btn">
            <a class="btn btn--wide" href="mailto:<?php echo $league['mail_to']; ?>">
              <span>CONTACT</span>
            </a>
          </div>
        </section>

        <?php if ( $teamowner['active'] ): ?>
        <section class="contact contact-teamowner">
          <p class="contact__description">
            <?php echo $teamowner['text']; ?>
          </p>
          <?php if ( $teamowner['pdf_file'] != '' ): ?>
          <div class="contact__pdf">
            <a href="<?php echo $teamowner['pdf_file']; ?>" target="_blank">
              <?php echo $teamowner['pdf_link_text']; ?>
            </a>
          </div>
          <?php endif; ?>
          <div class="contact__btn">
            <a class="btn btn--wide" href="mailto:<?php echo $teamowner['mail_to']; ?>">
              <span>CONTACT</span>
            </a>
          </div>
        </section>
        <?php endif; ?>
      </div>
    </div>


    <?php
      get_footer();
    ?>

  </div>
  </body>
  </html>
