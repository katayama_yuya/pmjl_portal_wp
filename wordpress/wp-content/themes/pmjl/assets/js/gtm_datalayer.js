
window.dataLayer = window.dataLayer || [];

var firstScroll = false;
var depthTargets = [];

$(window).on('load', function() {
  setTimeout(function () {
    // ページスクロールイベント発
    $('.gtm_depth').each(function(idx, elm){
      var $tgt = $(this);
      depthTargets.push({
        depth: $tgt.get( 0 ).offsetTop,
        event: $tgt.data('gtmev'),
        sent: false
      })
    })
  }, 1500);
});

$(window).on('load', function() {
  // クリックイベント発火
  $('.gtm_click').on('click', function(){
    var $tgt = $(this);
    dataLayer.push({
      'event' : $tgt.data('gtmev')
    });
  });

  $(window).on('scroll', function() {
    var scroll = $(window).scrollTop() + $(window).height();
    if ( !firstScroll && scroll > $(window).height() ) {
      dataLayer.push({
        'event' : '[PAGE DEPTH] Bellow 1st view'
      });
      firstScroll = true;
    }
    for(var i = 0; i < depthTargets.length; i++ ) {
      if( !depthTargets[i].sent && scroll > depthTargets[i].depth ) {
        dataLayer.push({
          'event' : depthTargets[i].event
        });
        depthTargets[i].sent = true;
      }
    }
  });
});
