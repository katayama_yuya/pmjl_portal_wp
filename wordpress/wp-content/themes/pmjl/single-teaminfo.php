<?php
get_header();
?>

<body <?php body_class(); ?>>
<div class="container">
  <?php
    include locate_template( 'template-parts/common/header_menu.php' );
    ?>

  <div class="content">
  <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    <div class="news wow fadeIn">

      <h1 class="ttl">
        <span class="ttl__en">TEAMS</span>
        <span class="ttl__ja">チーム紹介</span>
      </h1>


      <article class="team-info">
            
         <?php if(have_rows('team_info')): ?>
          <?php while(have_rows('team_info')): the_row(); ?>

              <div class="team-info__name">
                <img src="<?php the_sub_field('team_logo'); ?>" alt="<?php the_sub_field('team_name'); ?>">
                <?php the_sub_field('team_name'); ?>
              </div>
              <div>
                <?php the_sub_field('team_introduction'); ?>
              </div>
            </div>
          <?php endwhile; ?>
          <?php endif; ?>

        <?php if(have_rows('player')): ?>
          <?php while(have_rows('player')): the_row(); ?>
            <div class="player__item">
              <div class="player__img">
                <img src="<?php the_sub_field('player_img'); ?>" alt="<?php the_sub_field('player_name'); ?>">
              </div>
                <p class="player__name"><?php the_sub_field('player_name'); ?></p>
                <a class="player__link" href="https://twitter.com/<?php the_sub_field('twitter'); ?>" target="_blank">@<?php the_sub_field('twitter'); ?></a>
            </div>
          <?php endwhile; ?>
        <?php endif; ?>
      </article>

    </div>
  <?php endwhile; ?>
<?php endif; ?>

  </div>

  <a href="<?php echo home_url('/teaminfo'); ?>">チーム一覧ページ</a>

  <?php
    get_footer();
  ?>

</div>
</body>
</html>
