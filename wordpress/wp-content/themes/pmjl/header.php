<?php
header("X-XSS-Protection: 0");

$metaDescription = '目指せ頂点、そして世界へ2020年秋、日本最強チームが決まるここから世界へのサクセスストーリーが始まる';
$metaKeyword = 'PUBG,mobile,モバイル,大会,世界,賞金,エントリー';
$metaAuthor = 'PMJL SEASON0 - PUBG MOBILE JAPAN LEAGUE SEASON 0';
$ogpType = 'article';
$title = 'PMJL SEASON0 - PUBG MOBILE JAPAN LEAGUE SEASON 0';
$ogpTitle = 'PMJL SEASON0 - PUBG MOBILE JAPAN LEAGUE SEASON 0';
$ogpUrl = htmlspecialchars( get_current_url(), ENT_QUOTES );
$ogpImg = returnAssets('img') . '/common/ogp.jpg';
$twCard = 'summary';
$twSite = '@PUBGMOBILE_JP';
$appCss = returnAssets('css') . '/app.css';
$correctCss = returnAssets('css') . '/correct.css';

// ------------------ TOP ------------------------------------------------
if( is_home() ) :
  $ogpType = 'website';
// ------------------ 記事一覧 ------------------------------------------------
elseif( is_archive('news') ):
  $title = 'NEWS ｜PUBG MOBILE JAPAN LEAGUE SEASON 0';
  $ogpTitle = 'NEWS ｜PUBG MOBILE JAPAN LEAGUE SEASON 0';
// ------------------ 記事詳細 ------------------------------------------------
elseif( is_single() ):
  global $post;
  $pid = $post->ID;
  $title = strip_tags(get_the_title($pid)).' NEWS ｜PUBG MOBILE JAPAN LEAGUE SEASON 0';
  $ogpTitle = strip_tags(get_the_title($pid)).' NEWS ｜PUBG MOBILE JAPAN LEAGUE SEASON 0';
// ------------------ 固定ページ ------------------------------------------------
elseif( is_page('teamstats') ):
  $title = 'TEAM STATS ｜PUBG MOBILE JAPAN LEAGUE SEASON 0';
  $ogpTitle = 'TEAM STATS ｜PUBG MOBILE JAPAN LEAGUE SEASON 0';
elseif( is_page('contact') ):
  $title = 'CONTACT ｜PUBG MOBILE JAPAN LEAGUE SEASON 0';
  $ogpTitle = 'CONTACT ｜PUBG MOBILE JAPAN LEAGUE SEASON 0';
elseif( is_page('entry') ):
  $title = '>PUBG MOBILE JAPAN LEAGUE SEASON 0 大会エントリー募集中！/PUBG モバイル';
  $ogpTitle = '>PUBG MOBILE JAPAN LEAGUE SEASON 0 大会エントリー募集中！/PUBG モバイル';
  $metaDescription = '目指せ頂点、そして世界へ2020年秋、日本最強チームが決まるここから世界へのサクセスストーリーが始まる';
  $metaKeyword = 'PUBG, mobile, モバイル, 大会, 世界, 賞金, エントリー, PMJL, Season0';
  $ogpImg = returnAssets('entry/images') . '/common/ogp.jpg';
  $appCss = returnAssets('entry/css') . '/style.css?ver=1.0.1';
elseif( is_page() ):
endif;
wp_reset_postdata();
?><!DOCTYPE html>
<html dir="ltr" lang="ja">
<head>
  <meta charset="UTF-8">
  <title><?php echo $title; ?></title>
  <meta name="viewport" content="width=device-width">
  <meta name="description" content="<?php echo $metaDescription; ?>">
  <meta name="keywords" content="<?php echo $metaKeyword; ?>">
  <meta name="author" content="<?php echo $metaAuthor; ?>">

  <meta property="og:title" content="<?php echo $ogpTitle; ?>">
  <meta property="og:type" content="<?php echo $ogpType; ?>">
  <meta property="og:url" content="<?php echo $ogpUrl; ?>">
  <meta property="og:description" content="<?php echo $metaDescription; ?>">
  <meta property="og:image" content="<?php echo $ogpImg; ?>">
  <meta property="og:site_name" content="<?php echo $title; ?>">
  <meta property="og:locale" content="ja_JP">
  <meta name="twitter:card" content="<?php echo $twCard; ?>">
  <meta name="twitter:site" content="<?php echo $twSite; ?>">
  <meta name="twitter:title" content="<?php echo $ogpTitle; ?>">
  <meta name="twitter:description" content="<?php echo $metaDescription; ?>">
  <meta name="twitter:image" content="<?php echo $ogpImg; ?>">

  <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="<?php echoAssets('icons'); ?>/icon_black.png">
  <link rel="icon" type="image/png" href="<?php echoAssets('icons'); ?>/icon_black.png">
  <meta name="msapplication-TileColor" content="#5588ff">
  <meta name="theme-color" content="#ffffff">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php wp_head(); ?>
  <link rel="stylesheet" href="<?php echo $appCss ?>">
  <link rel="stylesheet" href="<?php echo $correctCss ?>">
  <script src="<?php echoAssets('js'); ?>/viewport.js"></script>
  <script defer src="<?php echoAssets('js'); ?>/app.js?ver=1.1.0"></script>
  <?php
  include locate_template( 'template-parts/common/gtm_before.php' );
  ?>
</head>
