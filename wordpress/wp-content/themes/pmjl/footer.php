<footer class="footer">
  <a href="#" class="footer__pagetop">
    <span></span>
    PAGE TOP
  </a>
  <a class="footer__logo" href="https://pubgmobile.jp/" target="_blank">
    <img src="<?php echoAssets('img'); ?>/common/logo-mobile.png" alt="PUBG MOBILE">
  </a>
  <p class="footer__copy">
    <img src="<?php echoAssets('img'); ?>/common/copy.svg" alt="© 2020 PUBG Corporation. All Rights Reserved.">
  </p>
</footer>

<script src="<?php echoAssets('js'); ?>/jquery-3.5.1.min.js"></script>
<script src="<?php echoAssets('js'); ?>/gtm_datalayer.js"></script>
<?php
include locate_template( 'template-parts/common/gtm_after.php' );
?>
