<?php
get_header();

$info_q = new WP_Query(
  array(
    'post_type'=>'teaminfo',
    'post_status' => 'publish',
  )
);
?>

<body <?php body_class(); ?>>
<div class="container">
  <?php
    include locate_template( 'template-parts/common/header_menu.php' );
    ?>

  <div class="content">

    <div class="news wow fadeIn">

      <h1 class="ttl">
        <span class="ttl__en">TEAM INFO</span>
        <span class="ttl__ja sp-hide">出場チーム一覧</span>
      </h1>

      <section class="news-list">
        <ul class="news-list__list">

          <?php if ($info_q->have_posts()) : ?>
            <?php while ($info_q->have_posts()) : $info_q->the_post(); ?>

            <?php if(have_rows('team_info')): ?>
              <?php while(have_rows('team_info')): the_row(); ?>
                
                <li class="news-list__item">
                <a class="news-list__link" href="<?php the_permalink(); ?>">
                <div>
                <div class="news-list__img">
                    <img src="<?php the_sub_field('team_img'); ?>" alt="<?php the_sub_field('team_name'); ?>">
                </div>

                <div class="news-list__name">
                    <img src="<?php the_sub_field('team_logo'); ?>" alt="<?php the_sub_field('team_name'); ?>">
                    <?php the_sub_field('team_name'); ?>
                </div>
                </div>
                </a>
                </li>
              <?php endwhile; ?>
            <?php endif; ?>

            <?php endwhile; ?>
          <?php endif; ?>
        </ul>
      </section>
    </div>
  </div>

  <?php
  get_footer();
  ?>

</div>
</body>
</html>