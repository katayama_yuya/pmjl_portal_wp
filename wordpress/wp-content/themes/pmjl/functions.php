<?php

global $wp_rewrite;
$wp_rewrite->flush_rules();
/**
 * PMJL functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage PMJL
 * @since PMJL 1.0
 */

function pmjl_theme_support() {
  add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'pmjl_theme_support' );

function twentytwenty_classic_editor_styles() {
	$classic_editor_styles = array(
    '/assets/css/app.css',
		'/assets/css/editor-style-classic.css'
	);
	add_editor_style( $classic_editor_styles );
}
add_action( 'init', 'twentytwenty_classic_editor_styles' );

/*--------------------------------------------------------------------*/
/*	カスタマイズ　ここから
/*-------------------------------------------------------------------*/

/*
 * devMode (STG環境フラグ)
 */
 ini_set( 'display_errors', 0 );
 $devMode = false;
 if( $_SERVER["HTTP_HOST"] === 'https://stg.pmjl.net/' ){
   $devMode = true;
 }

 /*
  * SP 判定
  */
 $sp = FALSE;
 if (( strpos( $_SERVER['HTTP_USER_AGENT'] , 'iPhone' ) !== FALSE )||( strpos( $_SERVER['HTTP_USER_AGENT'] , 'Android' ) !== FALSE )){
   $sp  = TRUE;
 }

/*
 * assets パス呼び出し
 */
function echoAssets($type){
	$dir = get_bloginfo('template_directory').'/assets/'.$type;
	echo $dir;
}
function returnAssets($type){
	$dir = get_bloginfo('template_directory').'/assets/'.$type;
	return $dir;
}
function assetsShortcode($atts) {
  extract(shortcode_atts(array(
      'type' => 'images/'
  ), $atts));
	$content = strip_tags($content);
	return get_bloginfo('template_directory').'/assets/'.$type.'/';
}
add_shortcode('assetsPath', 'assetsShortcode');

/*
 * 現在のURLを取得
 */
function get_current_url() {
  return ( is_ssl() ? 'https' : 'http' . '://' ) . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
}

/*
 * カスタム投稿タイプ
 */
function set_custom_post_type(){
  register_post_type( 'editable_contents',
    array(
      'label' => 'コンテンツ編集',
      'public' => true,
      'has_archive' => false,
      'menu_position' => 5,
      'supports' => array('title','editor','thumbnail','revisions')
    )
  );

  register_post_type( 'news',
    array(
      'label' => 'NEWS',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 6,
      'supports' => array('title','editor','thumbnail','revisions')
    )
  );

  register_post_type( 'stats',
    array(
      'label' => 'TEAM STATS',
      'public' => true,
      'has_archive' => false,
      'menu_position' => 7,
      'supports' => array('title','editor','thumbnail','revisions')
    )
  );

  register_post_type( 'teaminfo',
    array(
      'label' => 'TEAM INFO',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 8,
      'supports' => array('title','editor','thumbnail','revisions')
    )
  );
}
add_action('init', 'set_custom_post_type');

/*
 * 管理画面メニューカスタマイズ
 */
add_action( 'admin_menu', 'remove_menus' );
function remove_menus(){
    remove_menu_page( 'index.php' ); //ダッシュボード
    remove_menu_page( 'edit.php' ); //投稿メニュー
    // remove_menu_page( 'upload.php' ); //メディア
    // remove_menu_page( 'edit.php?post_type=page' ); //ページ追加
    remove_menu_page( 'edit-comments.php' ); //コメントメニュー
    //remove_menu_page( 'themes.php' ); //外観メニュー
    // remove_menu_page( 'plugins.php' ); //プラグインメニュー
    // remove_menu_page( 'tools.php' ); //ツールメニュー
    // remove_menu_page( 'options-general.php' ); //設定メニュー
}

/*
 * 管理バー強制非表示
 */
add_filter( 'show_admin_bar', '__return_false' );

/*
 * PREV, NEXT にクラス付与
 */
add_filter( 'previous_post_link', 'add_prev_post_link_class' );
function add_prev_post_link_class($output) {
  return str_replace('<a href=', '<a class="news-nav__link" href=', $output);
}
add_filter( 'next_post_link', 'add_next_post_link_class' );
function add_next_post_link_class($output) {
  return str_replace('<a href=', '<a class="news-nav__link" href=', $output);
}

/*
 * 曜日コードで曜日出力
 */
function get_weekday_j($w) {
  $week = [
    '日', //0
    '月', //1
    '火', //2
    '水', //3
    '木', //4
    '金', //5
    '土', //6
  ];
  return $week[$w];
}

/*
 * YouTube URL から ID を取得
 */
function get_youtube_id($url) {
    $pattern =
        '%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group Host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End Host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x'
        ;
    $result = preg_match($pattern, $url, $matches);
    if ($result) {
        return $matches[1];
    }
    return false;
}

/*
 * YouTube ID から サムネイル を取得
 */
 function get_youtube_thumbnail($id) {
  $maxurl = "https://i.ytimg.com/vi/".$id."/maxresdefault.jpg";
  $maximg_size = getimagesize($maxurl);
  if( $maximg_size[0] > 120 ) {
    return $maxurl;
  } else {
    return "https://i.ytimg.com/vi/".$id."/mqdefault.jpg";
  }
 }
 /*
  * YouTube URL から 動画タイトル を取得
  */
function get_youtube_title($url) {
  $youtube = "https://www.youtube.com/oembed?url=". $url ."&format=json";
  $curl = curl_init($youtube);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  $curl_return = curl_exec($curl);
  curl_close($curl);
  $video_data = json_decode($curl_return, true);
  return $video_data['title'];
}

/*
 * CSV から配列を生成
 */
function get_array_from_csv( $csv_url ) {
  $csv_path = mb_substr( parse_url($csv_url, PHP_URL_PATH) , 1 );
  $f = fopen($csv_path, "r");
  $idx = 0;
  $keys = [];
  $arr = [];
  while ($row = fgetcsv($f) ) :
    if ( $idx == 0 ) :
      $keys = $row;
    else :
      $arr_row = [];
      $j = 0;
      $arr_row['idx'] = $idx;
      foreach ($row as $cell) :
        $arr_row[$keys[$j]] = $cell;
        $j++;
      endforeach;
      array_push($arr, $arr_row);
    endif;
    $idx++;
  endwhile;
  return $arr;
}


/*
 * WP 及びプラグイン更新用
 */
function set_fs_method($args) {
  return 'direct';
}
add_filter('filesystem_method','set_fs_method');

/*
 * wp_headで出力される不要タグを削除
 */
remove_action( 'wp_head', '_wp_render_title_tag', 1 );
remove_action( 'wp_head', 'wp_generator');

/*
 * 不要な feed 排除
 */
remove_action('do_feed_rdf', 'do_feed_rdf');
remove_action('do_feed_rss', 'do_feed_rss');
remove_action('do_feed_rss2', 'do_feed_rss2');
remove_action('do_feed_atom', 'do_feed_atom');

/*
 * 絵文字無効化
 */
function disable_emojis() {
     remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
     remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
     remove_action( 'wp_print_styles', 'print_emoji_styles' );
     remove_action( 'admin_print_styles', 'print_emoji_styles' );
     remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
     remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
     remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
     //add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

/*
 * Gutenberg用 CSS 無効化
 */
function dequeue_plugins_style() {
    wp_dequeue_style('wp-block-library');
}
add_action( 'wp_enqueue_scripts', 'dequeue_plugins_style', 9999);
