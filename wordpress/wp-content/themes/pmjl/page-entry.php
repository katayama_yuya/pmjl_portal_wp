<?php
get_header();
?>


<body class="now-loading page-entry">
  <div class="wrapper" id="wrapper">
    <div class="global-header">
      <nav class="global-nav">
        <div class="head-logo"><a class="block-logo" href="../"><img class="img" src="<?php echoAssets('entry/images'); ?>logo_head.svg" alt="PUBG MOBILE JAPAN LEAGUE SEASON 0"></a></div>
        <ul class="list">
          <li class="list__item"><a class="link-btn link-btn--top" href="../"><span class="text">TOP</span></a></li>
          <li class="list__item"><a class="link-btn link-btn--entry" href="../entry/"><span class="text">ENTRY</span></a></li>
          <li class="list__item"><a class="link-btn link-btn--contact" href="../contact/"><span class="text">CONTACT</span></a></li>
        </ul>
      </nav>
      <nav class="external-nav">
        <ul class="list">
          <li class="list__item"><a class="link-btn" href="https://twitter.com/PUBGMOBILE_JP" target="_blank">
              <div class="ico"><svg xmlns="http://www.w3.org/2000/svg" width="66" height="66" viewBox="0 0 66 66"><path d="M25.57 49.39a23.36 23.36 0 0023.59-23.1v-1.61a18.46 18.46 0 004.06-4.26 18.45 18.45 0 01-4.72 1.34 8.68 8.68 0 003.58-4.49 20.39 20.39 0 01-5.15 2 8.11 8.11 0 00-6.08-2.7 8.45 8.45 0 00-8.32 8.32 4.37 4.37 0 00.22 1.79A23.23 23.23 0 0115.7 18a8.79 8.79 0 00-1.12 4.28 9 9 0 003.56 7 7.59 7.59 0 01-3.82-1.12 8.22 8.22 0 006.77 8.07 7.39 7.39 0 01-2.25.22 4.13 4.13 0 01-1.56-.22A8.51 8.51 0 0025.13 42a16.89 16.89 0 01-10.33 3.57 6 6 0 01-2-.22 21.13 21.13 0 0012.82 4" fill="#707070" fill-rule="evenodd"/></svg></div></a></li>
          <li class="list__item"><a class="link-btn" href="https://www.youtube.com/channel/UC9exSwJJO0ayLWSqo9-8dXg" target="_blank">
              <div class="ico"><svg xmlns="http://www.w3.org/2000/svg" width="66" height="66" viewBox="0 0 66 66"><path d="M53.84 24.47a9.16 9.16 0 00-1.69-4.23 6.2 6.2 0 00-4.26-1.8C42.07 18 33 18 33 18s-8.95 0-14.92.43a6.18 6.18 0 00-4.26 1.8 9.44 9.44 0 00-1.7 4.23 66.07 66.07 0 00-.33 6.89v3.23a66.2 66.2 0 00.42 6.9 9.33 9.33 0 001.7 4.23 7.2 7.2 0 004.7 1.83c3.39.32 14.47.44 14.47.44s9 0 14.91-.46a6 6 0 004.26-1.8 9.18 9.18 0 001.7-4.23 66.18 66.18 0 00.42-6.89v-3.24a66.18 66.18 0 00-.42-6.89m-27 15.85V24.74l15 7.82z" fill="#707070"/></svg></div></a></li>
        </ul>
      </nav>
    </div>
    <main class="main-container">
      <section class="area-entry">
        <div class="block-page-title">
          <h1 class="title--main"><img class="img" src="<?php echoAssets('entry/images'); ?>title_player_entry.svg" alt="PLAYER ENTRY"></h1>
          <p class="title--sub">プレイヤーエントリー</p>
        </div>
        <div class="block-entry">
          <p class="lead u-plat--sp">本大会に参加をご希望のチームは、<br class="hidden-pc">下記の「大会エントリー規約」に同意していただき、<br>エントリーフォーム入力にお進みください。</p>
          <div class="block-download"> <a class="link--download" href="assets/download/pmjls0_regulations.pdf" target="_blank"><span class="text">大会エントリー規約</span></a></div>
        </div>
        <div class="block-terms">
          <div class="block-textarea">
            <h3 class="heading">PUBG MOBILE JAPAN LEAGUE SEASON 0<br>大会エントリー規約</h3>
            <div class="block-content">
              <p class="note">株式会社NTTドコモ（以下「当社」といいます）が実施する「PUBG MOBILE JAPAN LEAGUE SEASON 0」(以下「本大会」といいます。）への参加にあたっては、この大会エントリー規約(以下「本規約」といいます。)および PUBG MOBILE JAPAN LEAGUE SEASON 0ルールブック(以下「ルールブック」といいます。)を必ずお読みください。参加者は、本大会の参加登録をもって、本規約およびルールブック（以下「本規約等」といいます）に同意したものといたします。</p>
              <ul class="list list--disc">
                <li class="list__item">なりすまし行為や、公序良俗に反する表現や行為、迷惑行為や不正があった場合は失格とします。</li>
                <li class="list__item">本大会のスチール撮影会及びGRAND FINAL参加者は、登録選手本人であることを確認できる本人確認書類を必ず持参の上、ご参加ください。</li>＊ 本大会における本人確認書類例は本規約の最後に掲載されております。
                <li class="list__item">本大会は、マスコミ各社による取材や撮影、インターネット配信、当社による記録撮影を予定しています。
                  <ul class="list list--dot">
                    <li class="list__item">記録物の主な用途は、公式ウェブサイトでの紹介の他、雑誌やWEB媒体での放映/掲載に加え、商用利用を予定しています。</li>
                    <li class="list__item">参加者は、全ての記録、またはその記録を使用して作られた作品や資料に関して、肖像権/著作権等の主張や行使をしない事とします。</li>
                  </ul>
                </li>
                <li class="list__item">事故/災害、その他やむを得ない理由により予告なく本大会が中止となった場合、当社は発生した損害に対して一切の責任を負いません。</li>
                <li class="list__item">エントリー期間は、2020年8月7日（金）-8月20日（木）23:59です。<br>＊ 募集期間は変更になる可能性がございます。</li>
                <li class="list__item">エントリーは、必ずご本人様にてお願いいたします。（複数回のエントリーは禁止とします。）</li>
                <li class="list__item">結果通知は、後日、ご登録いただいたメールアドレスにお送りいたします。
                  <ul class="list">
                    <li class="list__item">通知については、2020年8月21日(金)より2020年8月26日(水)までに大会運営チームよりお知らせいたします。</li>
                  </ul>
                </li>
                <li class="list__item">エントリー時点で満17歳以上の方がエントリーできます。</li>
                <li class="list__item">エントリー時点で20歳未満の場合は、保護者同意書が必要です。20歳未満の方は、保護者同意書を印刷し必要事項を記載のうえ、エントリーと合わせてご提出をお願いいたします。</li>
                <li class="list__item">ご家族やご友人など、他の方に当選権利を譲渡することは禁止とします。</li>
                <li class="list__item">エントリーの際に提供された参加者の個人情報は、本大会の開催、運営等の目的で使用いたします。その他個人情報の取扱いは、ルールブックに定めます。</li>
              </ul>
            </div>
            <h4 class="subhead">禁止事項</h4>
            <div class="block-content">
              <p class="note">参加者は、以下の行為をしてはいけません。これらの行為に該当する、またはこれらの行為がなされるおそれがあると当社が判断した場合、本大会への参加資格を取り消します。また、チーム内の特定の方が参加資格を取り消しとなった場合は、チームの参加資格を取り消します。</p>
              <ul class="list list--disc">
                <li class="list__item">大会運営スタッフの本大会進行上に必要な指示/要請に従わない行為。</li>
                <li class="list__item">大会関係者、参加者、または第三者に対して、誹謗/中傷する行為、名誉/信用を傷つける行為、暴力/ハラスメントなどの行為。</li>
                <li class="list__item">大会運営チーム、または第三者の知的財産権、肖像権、プライバシー、名誉、その他の権利、もしくは利益を侵害する行為。</li>
                <li class="list__item">大会運営チームへ虚偽の申請や報告を行う行為。</li>
                <li class="list__item">本大会への参加権、賞品、または賞品を受け取る権利を第三者へ譲渡、売買する行為。</li>
                <li class="list__item">本大会を利用し、営利を目的とする行為。</li>
                <li class="list__item">チート手法、不正にゲームを有利に進めるような機器またはプログラム、これに類似する信号、装置や手信号などの手法の使用、または、大会運営スタッフがチート行為と判断した場合。<br>＊ 指サック、パウダー、固定スタンドの使用は認められています。</li>
                <li class="list__item">故意の切断、適切かつ明示された理由によらない故意による切断。</li>
                <li class="list__item">複数のアカウントを利用して大会に参加する行為。</li>
                <li class="list__item">ゲームの不具合やバグなどを利用した行為。</li>
                <li class="list__item">ルールブックに記載してあることに反する行為。</li>
              </ul>
            </div>
            <h4 class="subhead">免責事項</h4>
            <div class="block-content">
              <ul class="list list--disc">
                <li class="list__item">当社のネットワーク環境の不具合やシステムメンテナンス等により参加登録ができなかった場合であっても、当社は責任を負いかねますので予めご了承ください。</li>
                <li class="list__item">スチール撮影会及びGRAND FINAL時の会場内での盗難、紛失、個人間のトラブルについて、当社は一切の責任を負いかねます。</li>
                <li class="list__item">ゲームサーバー、インターネット接続、機材トラブル、天災その他予測できない状況では、本大会を中断または中止する場合があります。その場合であっても、本大会の参加に伴う費用や損害の補填はいたしかねますので、ご了承ください。</li>
                <li class="list__item">本大会の運営に関して、当社の過失による債務不履行または不法行為により参加者に対して損害賠償責任を負う場合、当社が賠償する損害は通常生ずべき直接かつ現実の損害（逸失利益を除きます）に限るものとします。ただし、当該免責および本規約等の他の規定における当社の免責は、当社の故意または重大な過失による場合には適用しません。</li>
              </ul>
            </div>
            <h4 class="subhead">プライバシーポリシーについて</h4>
            <div class="block-content">
              <p class="note">NTTドコモによる個人情報の収集、使用、および開示に関する重要な情報については、<a href="https://www.nttdocomo.co.jp/utility/privacy/" target="_blank">https://www.nttdocomo.co.jp/utility/privacy/</a>に掲載されているNTTドコモのプライバシーポリシーを参照してください。</p>
            </div>
            <h4 class="subhead">その他</h4>
            <div class="block-content">
              <ul class="list list--disc">
                <li class="list__item">本規約等に含まれていない事項については、当社および大会運営チームの決定に従っていただきます。</li>
                <li class="list__item">当社は、次に掲げる場合、あらかじめ本大会のウェブサイトに掲載するなど、当社が適切と判断する方法により周知をすることにより、本規約の内容を変更することができるものとします。本規約が変更された場合、変更日以降は、当該変更後の本規約が、すべての参加者と当社との間で適用されるものとします。
                  <ol class="list">
                    <li class="list__item">①本規約の変更が、参加者の一般の利益に適合するとき。</li>
                    <li class="list__item">②本規約の変更が、参加者による本大会への参加に係る目的に反せず、かつ、変更の必要性、変更後の内容の相当性その他の変更に係る事情に照らして合理的なものであるとき。</li>
                  </ol>
                </li>
              </ul>
            </div>
            <h4 class="subhead">参考：本人確認書類例</h4>
            <div class="block-content">
              <p class="note">大会開催日に有効な物のみが、本人確認書類として認められます。有効期間外の本人確認書類は、いかなる理由をもっても本人確認書類としては認められません。</p>
              <ul class="list list--disc">
                <li class="list__item">1枚で本人確認書類として認められるもの　※顔写真付き
                  <ul class="list list--dot">
                    <li class="list__item">自動車運転免許証</li>
                    <li class="list__item">旅券（パスポート）</li>
                    <li class="list__item">住民基本台帳カード</li>
                    <li class="list__item">特別永住者証明書</li>
                    <li class="list__item">国もしくは地方公共団体の機関が発行した身分証明書（写真付）</li>
                    <li class="list__item">学生証(写真付)</li>
                  </ul>
                </li>
                <li class="list__item">2枚で本人確認書類として認められるもの
                  <ul class="list list--dot">
                    <li class="list__item">健康保険被保険者証</li>
                    <li class="list__item">国民健康保険被保険者証</li>
                    <li class="list__item">国民年金手帳</li>
                    <li class="list__item">印鑑登録証明書</li>
                    <li class="list__item">住民票の写しもしくは住民票記載事項証明書</li>
                    <li class="list__item">戸籍の附票の写し（謄本若しくは抄本）</li>
                    <li class="list__item">学生証(写真が無い場合)</li>
                  </ul>
                </li>
              </ul>
            </div>
            <h4 class="subhead">参考：エントリー入力項目</h4>
            <div class="block-content">
              <p class="note">エントリー時には、下記項目について登録が必要です。エントリー登録後の登録情報の変更につきましては、原則ご対応できません。入力間違いには十分ご注意ください。</p>
              <ul class="list list--disc">
                <li class="list__item">氏名</li>
                <li class="list__item">生年月日</li>
                <li class="list__item">現在居住地(都道府県)</li>
                <li class="list__item">保護者同意書の写し(※20歳未満の方)</li>
                <li class="list__item">チーム名（※チームメンバー統一要）</li>
                <li class="list__item">チーム名の読み方（カタカナ）（※チームメンバー統一要）</li>
                <li class="list__item">チーム構成人数</li>
                <li class="list__item">希望チーム名呼称（英数字大文字2～3文字）（※チームメンバー統一要）</li>
                <li class="list__item">選手活動名</li>
                <li class="list__item">選手活動名の読み方(カタカナ)</li>
                <li class="list__item">PUBG MOBILEのゲームキャラクター名</li>
                <li class="list__item">PUBG MOBILEのゲームキャラクター名の読み方（カタカナ）</li>
                <li class="list__item">キャラクター ID (数字)</li>
                <li class="list__item">Twitter ID (例：@PUBGMOBILE_JP)</li>
                <li class="list__item">過去実績（例：PMJCseason2ファイナル進出）</li>
                <li class="list__item">本大会に使用されるPUBG MOBILE用端末の機種名（例：GalaxyS20+ ）</li>
                <li class="list__item">事務局連絡用メールアドレス</li>
                <li class="list__item">チームリーダーのキャラクターID (数字)</li>
                <li class="list__item">あなたはチームリーダー又はチームマネージャーですか？</li>
                <li class="list__item">チームリーダー / チームマネージャーが選手を兼任するか</li>
                <li class="list__item">チームの意気込みコメント（※チームリーダー又はチームマネージャーのみ）</li>
                <li class="list__item">チームロゴ（※作成されたロゴがある場合のみ）（※チームリーダー又はチームマネージャーのみ）</li>
                <li class="list__item">Discord ID (例 : Admin#1234)（※チームリーダー又はチームマネージャーのみ）</li>
                <li class="list__item">オフライン実施時会場エリア（第一希望）（関東、関西、九州中1カ所記載）（※チームメンバー統一要）</li>
                <li class="list__item">オフライン実施時会場エリア（第二希望）（関東、関西、九州中1カ所記載）（※チームメンバー統一要）</li>
                <li class="list__item">オフライン実施時会場エリア（第三希望）（関東、関西、九州中1カ所記載）（※チームメンバー統一要）</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="block-parent">
          <h2 class="heading">20歳未満の参加者の方へ</h2>
          <p class="note">エントリー時点で20歳未満の場合は、保護者同意書が必要です。<br>20歳未満の方は、保護者同意書を印刷し必要事項を記載のうえ、<br class="hidden-sp">エントリーと合わせてご提出をお願いいたします。</p>
          <div class="block-download"> <a class="link--download" href="assets/download/pmjls0_consent.pdf" target="_blank"><span class="text">保護者同意書（PDF）</span></a></div>
        </div>
        <div class="block-agreement">
          <div class="block-checkbox">
            <div class="box-check">
              <div class="block-btn"><span class="link--btn"><span class="text">エントリー受付は終了しました</span></span></div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <footer class="global-footer"><a class="foot-logo" href="https://pubgmobile.jp/" target="_blank"><img class="img" src="<?php echoAssets('entry/images'); ?>logo_pubgm_mini.png" alt="PUBG MOBILE"></a>
      <p class="copyright">© 2020 PUBG Corporation. All Rights Reserved.</p>
    </footer>
  </div>
</body>

  </div>
  </body>
  </html>
