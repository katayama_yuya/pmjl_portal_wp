<?php
get_header();
$default_stage = $_POST['stage'];
?>

<body <?php body_class(); ?>>
  <div class="container">
    <?php
    include locate_template( 'template-parts/common/header_menu.php' );
    ?>

    <div class="content">

      <section class="head">
        <p class="head__logo">
          <picture>
            <source media="(max-width : 765px)" srcset="<?php echoAssets('img'); ?>/common/logo.svg">
            <img class="head__img" src="<?php echoAssets('img'); ?>/common/head-logo.png" alt="JAPAN LEAGUE PUBG MOBILE SEASON0">
          </picture>
        </p>
      </section>

      <?php
      $ts_stages = array(
        'ts_stage1',
        'ts_stage2',
        'ts_stage3',
        'ts_semifinal',
        'ts_grandfinal',
      );
      $ts_sd = array();
      $ts_gr = array();
      $ever_active = true;
      foreach( array_reverse($ts_stages) as $s ) {
        $tmp_pid = get_page_by_path($s, 'OBJECT', 'stats')->ID;
        $ts_sd[$s] = get_field('stats_default', $tmp_pid );
        $ts_gr[$s] = get_field('group_ranking', $tmp_pid );
        if( $default_stage != '' ) {
          if( $ever_active && $s == $default_stage ) {
            $ts_sd[$s]['newest'] = true;
            $ever_active = false;
          } else {
            $ts_sd[$s]['newest'] = false;
          }
        } else {
          if( $ever_active && $ts_sd[$s]['active'] ) {
            $ts_sd[$s]['newest'] = true;
            $ever_active = false;
          } else {
            $ts_sd[$s]['newest'] = false;
          }
        }
      }
      ?>

      <div class="team">

        <h1 class="ttl">
          <span class="ttl__en">TEAM STATS</span>
          <span class="ttl__ja">チームスタッツ・成績</span>
        </h1>

        <nav id="teamNav" class="stage-nav">
          <ul class="stage-nav__list">
            <li data-round="ts_stage1"
              class="stage-nav__item<?php if(!$ts_sd['ts_stage1']['active']): ?> disable<?php endif;  if($ts_sd['ts_stage1']['newest']): ?> active<?php endif ?>">
              GROUP<br>STAGE 1
            </li>
            <li data-round="ts_stage2"
              class="stage-nav__item<?php if(!$ts_sd['ts_stage2']['active']): ?> disable<?php endif;  if($ts_sd['ts_stage2']['newest']): ?> active<?php endif ?>">
              GROUP<br>STAGE 2
            </li>
            <li data-round="ts_stage3"
              class="stage-nav__item<?php if(!$ts_sd['ts_stage3']['active']): ?> disable<?php endif;  if($ts_sd['ts_stage3']['newest']): ?> active<?php endif ?>">
              GROUP<br>STAGE 3
            </li>
            <li data-round="ts_semifinal"
              class="stage-nav__item<?php if(!$ts_sd['ts_semifinal']['active']): ?> disable<?php endif;  if($ts_sd['ts_semifinal']['newest']): ?> active<?php endif ?>">
              SEMI<br>FINAL
            </li>
            <li data-round="ts_grandfinal"
              class="stage-nav__item<?php if(!$ts_sd['ts_grandfinal']['active']): ?> disable<?php endif;  if($ts_sd['ts_grandfinal']['newest']): ?> active<?php endif ?>">
              GRAND<br>FINAL
            </li>
          </ul>
        </nav>

        <section class="team-ttl">
          <div class="team-ttl__icon">
            <picture>
              <source media="(orientation: landscape) and (max-width : 765px)" srcset="<?php echoAssets('img'); ?>/team/crown.svg">
              <img class="team-ttl__icon-img" src="<?php echoAssets('img'); ?>/team/crown.svg" alt="">
            </picture>
          </div>
          <h2 id="teamTitle" class="team-ttl__text">
            <?php
            foreach( $ts_stages as $s ) :
              $sds = $ts_sd[$s];
            ?>
              <span <?php if($ts_sd[$s]['newest']): ?>class="active" <?php endif; ?>
                data-round="<?php echo $s; ?>">
                <?php echo $sds['winner']; ?>
              </span>
            <?php
            endforeach;
            ?>
          </h2>
        </section>

        <nav id="teamSelect" class="team-select">
          <div class="team-select__wrap">
            <?php
            foreach( $ts_stages as $s ) :
              $sds = $ts_sd[$s];
              $grs = $ts_gr[$s];
            ?>
              <ul data-round="<?php echo $s; ?>" class="team-select__list active">
                <li data-round="<?php echo $s; ?>" data-group="1" class="team-select__item active">
                  TOTAL RANKING
                </li>
                <?php
                $i = 2;
                foreach( $grs as $g ) :
                ?>
                  <li data-round="<?php echo $s; ?>" data-group="<?php echo $i; ?>" class="team-select__item">
                    <?php echo $g['group_text']; ?>
                  </li>
                <?php
                $i++;
                endforeach;
                ?>
              </ul>
            <?php
            endforeach;
            ?>
          </div>
        </nav>

        <section id="teamRanking" class="team-table">
          <?php
          foreach( $ts_stages as $s ) :
            $ph = ( preg_match('/final$/', $s) ) ? 'final' : 'group'; // フェーズ
            $sds = $ts_sd[$s];
            $grs = $ts_gr[$s];
          ?>
          <div data-round="<?php echo $s; ?>" data-group="1" class="team-rank">
            <div class="rank-<?php echo $ph; ?>">
              <table class="rank-<?php echo $ph; ?>__table">
                <tr class="rank-<?php echo $ph; ?>__head">
                  <th class="label-order">順位</th>
                  <?php if($ph == 'final'): ?>
                  <th class="label-icon"></th>
                  <?php endif; ?>
                  <th class="label-team">チーム名</th>
                  <th class="label-rank">ランクPT</th>
                  <th class="label-kill">キルPT</th>
                  <th class="label-total">合計PT</th>
                </tr>
                <?php
                $rank = 0;
                $csv_path = mb_substr( parse_url($sds['ranking_csv'], PHP_URL_PATH) , 1 );
                $f = fopen($csv_path, "r");
                while ($ranking = fgetcsv($f) ) :
                  if ( $rank > 0 ) :
                    $hl = ( $rank <= $sds['rank_highlight'] ); // ハイライト
                    $team_name =  mb_convert_encoding($ranking[0], 'UTF-8', 'SJIS-win');
                ?>
                  <tr class="rank-<?php echo $ph; ?>__row<?php if($hl): ?> active<?php endif; ?>">
                    <td class="cell-order"><?php echo $rank; ?></td>
                    <?php if($ph == 'final'): ?>
                      <td class="cell-icon"><img src="/wp-content/uploads/<?php echo $ranking[4]; ?>" alt="<?php echo $team_name; ?>"></td>
                      <?php endif; ?>
                      <td class="cell-team"><?php echo $team_name; ?></td>
                      <td class="cell-rank"><?php echo $ranking[1]; ?></td>
                      <td class="cell-kill"><?php echo $ranking[2]; ?></td>
                      <td class="cell-total"><?php echo $ranking[3]; ?></td>
                  </tr>
                <?php
                    endif;
                    $rank++;
                  endwhile;
                ?>
              </table>
            </div>
          </div>

            <?php
            $i = 2;
            foreach( $grs as $g ) :
                $hl = ( $rank <= $sds['rank_highlight'] ); // ハイライト
            ?>
            <div data-round="<?php echo $s; ?>" data-group="<?php echo $i; ?>" class="team-rank">
              <div class="rank-<?php echo $ph; ?>">
                <table class="rank-<?php echo $ph; ?>__table">
                  <tr class="rank-<?php echo $ph; ?>__head">
                    <th class="label-order">順位</th>
                    <?php if($ph == 'final'): ?>
                    <th class="label-icon"></th>
                    <?php endif; ?>
                    <th class="label-team">チーム名</th>
                    <th class="label-rank">ランクPT</th>
                    <th class="label-kill">キルPT</th>
                    <th class="label-total">合計PT</th>
                  </tr>
                  <?php
                    $rank = 0;
                    $csv_path = mb_substr( parse_url($g['ranking_csv'], PHP_URL_PATH) , 1 );
                    $f = fopen($csv_path, "r");
                    while ($ranking = fgetcsv($f) ) :
                      if ( $rank > 0 ) :
                      $team_name =  mb_convert_encoding($ranking[0], 'UTF-8', 'JIS, EUCJP-win, SJIS-win, UTF-8');
                    ?>
                    <tr class="rank-<?php echo $ph; ?>__row<?php if($hl): ?> active<?php endif; ?>">
                      <td class="cell-order"><?php echo $rank; ?></td>
                      <?php if($ph == 'final'): ?>
                      <td class="cell-icon"><img src="/wp-content/uploads/<?php echo $ranking[4]; ?>" alt="<?php echo $team_name; ?>"></td>
                      <?php endif; ?>
                      <td class="cell-team"><?php echo $team_name; ?></td>
                      <td class="cell-rank"><?php echo $ranking[1]; ?></td>
                      <td class="cell-kill"><?php echo $ranking[2]; ?></td>
                      <td class="cell-total"><?php echo $ranking[3]; ?></td>
                    </tr>
                    <?php
                      endif;
                      $rank++;
                    endwhile;
                  ?>
                </table>
              </div>
            </div>
            <?php
              $i++;
              endforeach;
            ?>
          <?php
          endforeach;
          ?>
        </section>

      </div>


      <script>
        document.addEventListener('DOMContentLoaded', function () {
          new app.Team();
        });
      </script>

      <?php
      get_footer();
    ?>

    </div>
</body>

</html>
