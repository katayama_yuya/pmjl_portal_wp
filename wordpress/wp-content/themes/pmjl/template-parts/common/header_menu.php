<?php
$top_content_pid = get_page_by_path('top_contents', 'OBJECT', 'editable_contents')->ID;
$top_content_mode = get_field('mode', $top_content_pid);
$held_flg = ( $top_content_mode != 'mode_a' );
$held_finals_flg = ( $top_content_mode == 'mode_b_final' || $top_content_mode == 'mode_c_final');

// ARCHIVE の有無チェック
$archives_pid = get_page_by_path('archives', 'OBJECT', 'editable_contents')->ID;
$stages = array(
  'stage1',
  'stage2',
  'stage3',
  'semifinal',
  'grandfinal',
);
$stages_name = array(
  'stage1' => 'GROUP STAGE 1',
  'stage2' => 'GROUP STAGE 2',
  'stage3' => 'GROUP STAGE 3',
  'semifinal' => 'SEMI FINAL',
  'grandfinal' => 'GRAND FINAL',
);
$g = array();
$ever_active = true;
$active_exist = false;
foreach(array_reverse($stages) as $s) {
  $g[$s] = get_field($s, $archives_pid );
  if( $g[$s]['active'] ) {
    $active_exist = true;
  }
  if( $ever_active && $g[$s]['active'] ) {
    $g[$s]['newest'] = true;
    $ever_active = false;
  } else {
    $g[$s]['newest'] = false;
  }
}
?>
  <header class="header">
    <div class="header__inner">
      <h1 class="header__logo">
        <a href="<?php echo home_url(); ?>" class="gtm_click" data-gtmev="[INTERNAL LINK] HEADER - Logo">
          <img src="<?php echoAssets('img'); ?>/common/logo.svg" alt="JAPAN LEAGUE PUBG MOBILE SEASON 0">
        </a>
      </h1>
      <nav>
        <ul class="header__list">
          <li class="header__item">
            <a href="<?php echo home_url(); ?>#program" class="header__link gtm_click"
              data-gtmev="[JUMP] HEADER - PROGRAM">
              PROGRAM
            </a>
          </li>
          <li class="header__item">
            <a href="<?php echo home_url(); ?>#schedule" class="header__link gtm_click"
              data-gtmev="[JUMP] HEADER - SCHEDULE">
              SCHEDULE
            </a>
          </li>
          <li class="header__item">
            <a href="<?php echo home_url(); ?>#archives" class="header__link gtm_click<?php if(!$held_flg || !$active_exist): ?> disable<?php endif ?>"
              data-gtmev="[JUMP] HEADER - ARCHIVES">
              ARCHIVES
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </header>
