
<section class="mv">
  <div class="mv__wrap">
    <picture>
      <source media="(max-width: 765px)" srcset="<?php echoAssets('img'); ?>/top/mv-sp.jpg">
      <img src="<?php echoAssets('img'); ?>/top/mv.jpg" alt="JAPAN REAGE PUBG MOBILE">
    </picture>
  </div>
  <script>
    window.addEventListener('load', function() {
      document.querySelector('.mv').classList.add('active');
    });
  </script>
</section>
