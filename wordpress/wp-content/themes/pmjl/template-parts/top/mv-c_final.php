<?php
$after_day = get_field('after_day', $top_content_pid );
$c_archives = $after_day['archive'];
$prompt = $after_day['prompt'];
?>
<section class="head">
  <p class="head__logo">
    <picture>
      <source media="(max-width : 765px)" srcset="<?php echoAssets('img'); ?>/common/logo.svg">
      <img class="head__img" src="<?php echoAssets('img'); ?>/common/head-logo.png" alt="JAPAN LEAGUE PUBG MOBILE SEASON0">
    </picture>
  </p>
</section>
<section class="recent">

  <div class="wow fadeIn">
    <h2 class="recent-ttl">
      <span class="recent-ttl__en"><?php echo $after_day['catch_en']; ?></span>
      <span class="recent-ttl__ja"><?php echo $after_day['catch_jp']; ?></span>
    </h2>


    <section class="recent-slide">
      <div class="recent-slide__wrap">
        <div class="swiper-container recent-slide__list">
          <div class="swiper-wrapper">
          <?php foreach($c_archives as $archive) :
            $yid = get_youtube_id($archive['youtube_link']);
            $ythumb = get_youtube_thumbnail($yid);
            ?>
            <div class="swiper-slide">
              <div class="recent-slide__item gtm_click" data-gtmev="[WATCH YT] RECENT - YT <?php echo $archive['text']; ?>" data-youtube-id="<?php echo $yid; ?>">
                <div class="recent-slide__img">
                  <img src="<?php echo $ythumb ?>" alt="<?php echo strip_tags($archive['text'];) ?>">
                  <div class="recent-slide__btn">
                    <img src="<?php echoAssets('img'); ?>/common/btn-yt.svg" alt="PLAY">
                  </div>
                </div>
                <h3 class="recent-slide__ttl"><?php echo $archive['text']; ?></h3>
              </div>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
        <div class="recent-slide__prev"></div>
        <div class="recent-slide__next active"></div>
      </div>
    </section>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        new app.Recent();
      });
    </script>


    <section id="recentBody" class="recent-body">

      <h3 class="recent-body__ttl"><?php echo $prompt['prompt_section']; ?></h3>
      <p class="recent-body__copy"><?php echo $prompt['prompt_title']; ?></p>
      <p class="recent-body__body">
        <?php echo $prompt['prompt_content']; ?><br>
      </p>
      <p class="recent-body__body disable">
        <?php echo $prompt['prompt_content_next']; ?>
      </p>
      <div class="recent-body__mask"></div>
    </section>

    <div id="recentMore" class="recent-more">
      <div class="recent-more__wrap gtm_click" data-gtmev="[CLICK] RECENT - Tap to read recent news">
        <div class="recent-more__img">
          <span></span>
          <span></span>
        </div>
        <p class="recent-more__txt">タップして速報を見る</p>
      </div>
    </div>

  </div>

</section>
