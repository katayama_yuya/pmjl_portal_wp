<?php
$partner_pid = get_page_by_path('partner', 'OBJECT', 'editable_contents')->ID;
?>
		<section class="partner gtm_depth" data-gtmev="[PAGE DEPTH] PARTNER">
      <div class="wow fadeIn">
        <p class="partner__logo">
          <img src="<?php echoAssets('img'); ?>/common/logo.svg" alt="JAPAN LEAGUE PUBG MOBILE SEASON 0">
        </p>
        <div class="ttl ttl--short">
          <h2 class="ttl__en">
            PARTNER
          </h2>
        </div>
        <div class="partner__wrap">

          <?php if(have_rows('logo', $partner_pid)): ?>
            <?php while(have_rows('logo', $partner_pid)): the_row(); ?>
            <h3 class="partner__img">
              <?php if (get_sub_field('url') != '') : ?>
              <a href="<?php the_sub_field('url'); ?>" target="_blank" class="gtm_click" data-gtmev="[EXTERNAL LINK] PARTNER <?php the_sub_field('url'); ?>">
                <img src="<?php the_sub_field('logo_image'); ?>" alt="<?php the_sub_field('name'); ?>">
              </a>
              <?php else: ?>
                <img src="<?php the_sub_field('logo_image'); ?>" alt="<?php the_sub_field('name'); ?>">
              <?php endif; ?>
            </h3>
            <?php endwhile; ?>
          <?php endif; ?>

        </div>
      </div>
    </section>
