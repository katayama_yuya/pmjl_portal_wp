<?php
$ts_stages = array(
  'ts_stage1',
  'ts_stage2',
  'ts_stage3',
  'ts_semifinal',
  'ts_grandfinal',
);
$ts_sd = array();
$ts_gr = array();
$ever_active = true;
foreach( array_reverse($ts_stages) as $s ) {
  $tmp_pid = get_page_by_path($s, 'OBJECT', 'stats')->ID;
  $ts_sd[$s] = get_field('stats_default', $tmp_pid );
  $ts_gr[$s] = get_field('group_ranking', $tmp_pid );
  $ts_post = get_post($tmp_pid);
  $ts_sd[$s]['title'] = get_the_title($ts_post);
  if( $ever_active && $ts_sd[$s]['active'] ) {
    $ts_sd[$s]['newest'] = true;
    $ever_active = false;
  } else {
    $ts_sd[$s]['newest'] = false;
  }
}
?>

<section id="teamstats" class="team-top gtm_depth" data-gtmev="[PAGE DEPTH] TEAM STATS">

  <div class="wow fadeIn">
    <h1 class="ttl">
      <span class="ttl__en">TEAM STATS</span>
      <span class="ttl__ja">チームスタッツ・成績</span>
    </h1>

    <nav id="teamNav" class="stage-nav stage-nav--nallow">
      <div class="stage-ctl">
        <span class="btn-prev"><img class="img" src="<?php echoAssets('img'); ?>/common/stage-nav-arw-l.svg"></span>
        <span class="btn-next"><img class="img" src="<?php echoAssets('img'); ?>/common/stage-nav-arw-r.svg"></span>
      </div>
      <ul class="stage-nav__list">
        <li data-round="ts_stage1"
          class="stage-nav__item<?php if(!$ts_sd['ts_stage1']['active']): ?> disable<?php endif;  if($ts_sd['ts_stage1']['newest']): ?> active<?php endif ?> gtm_click" data-gtmev="[CLICK] TEAM STATS Tab - <?php echo $ts_sd['ts_stage1']['title'] ?>">
          GROUP<br>STAGE 1
        </li>
        <li data-round="ts_stage2"
          class="stage-nav__item<?php if(!$ts_sd['ts_stage2']['active']): ?> disable<?php endif;  if($ts_sd['ts_stage2']['newest']): ?> active<?php endif ?> gtm_click" data-gtmev="[CLICK] TEAM STATS Tab - <?php echo $ts_sd['ts_stage2']['title'] ?>">
          GROUP<br>STAGE 2
        </li>
        <li data-round="ts_stage3"
          class="stage-nav__item<?php if(!$ts_sd['ts_stage3']['active']): ?> disable<?php endif;  if($ts_sd['ts_stage3']['newest']): ?> active<?php endif ?> gtm_click" data-gtmev="[CLICK] TEAM STATS Tab - <?php echo $ts_sd['ts_stage3']['title'] ?>">
          GROUP<br>STAGE 3
        </li>
        <li data-round="ts_semifinal"
          class="stage-nav__item<?php if(!$ts_sd['ts_semifinal']['active']): ?> disable<?php endif;  if($ts_sd['ts_semifinal']['newest']): ?> active<?php endif ?> gtm_click" data-gtmev="[CLICK] TEAM STATS Tab - <?php echo $ts_sd['ts_semifinal']['title'] ?>">
          SEMI<br>FINAL
        </li>
        <li data-round="ts_grandfinal"
          class="stage-nav__item<?php if(!$ts_sd['ts_grandfinal']['active']): ?> disable<?php endif;  if($ts_sd['ts_grandfinal']['newest']): ?> active<?php endif ?> gtm_click" data-gtmev="[CLICK] TEAM STATS Tab - <?php echo $ts_sd['ts_grandfinal']['title'] ?>">
          GRAND<br>FINAL
        </li>
      </ul>
    </nav>

    <section id="teamRanking" class="team-table team-table--top">

      <?php
      foreach( $ts_stages as $s ) :
        $ph = ( preg_match('/final$/', $s) ) ? 'final' : 'group'; // フェーズ
        $g_fields = $ts_sd[$s];
      ?>
      <div data-round="<?php echo $s; ?>" data-group="1" class="team-rank">
        <div class="rank-<?php echo $ph; ?>">
          <table class="rank-<?php echo $ph; ?>__table">
            <tr class="rank-<?php echo $ph; ?>__head">
              <th class="label-order">順位</th>
              <th class="label-team">チーム名</th>
              <th class="label-rank">ランクPT</th>
              <th class="label-kill">キルPT</th>
              <th class="label-total">合計PT</th>
            </tr>
            <?php
              $rank = 0;
              $csv_path = mb_substr( parse_url($g_fields['ranking_csv'], PHP_URL_PATH) , 1 );
              $f = fopen($csv_path, "r");
              while ($ranking = fgetcsv($f) ) :
                if ( $rank > 0 ) :
                $hl = ( $rank <= $g_fields['rank_highlight'] ); // ハイライト
                $team_name =  mb_convert_encoding($ranking[0], 'UTF-8', 'JIS, EUCJP-win, SJIS-win, UTF-8');
            ?>
            <tr class="rank-<?php echo $ph; ?>__row<?php if($hl): ?> active<?php endif; ?>">
              <td class="cell-order"><?php echo $rank; ?></td>
              <td class="cell-team"><?php echo $team_name; ?></td>
              <td class="cell-rank"><?php echo $ranking[1]; ?></td>
              <td class="cell-kill"><?php echo $ranking[2]; ?></td>
              <td class="cell-total"><?php echo $ranking[3]; ?></td>
            </tr>
            <?php
                endif;
                $rank++;
              endwhile;
            ?>
          </table>

        </div>
        <form method="post" action="teamstats" name="form_<?php echo $s; ?>">
          <input type="hidden" name="stage" value="<?php echo $s; ?>">
          <a type="submit" class="btn btn--small btn--arr gtm_click" data-gtmev="[INTERNAL LINK] TEAM STATS - MORE" href="javascript:form_<?php echo $s; ?>.submit()">
            <span>MORE</span>
          </a>
        </form>
      </div>
      <?php
        endforeach;
      ?>

    </section>

    <script>
      document.addEventListener('DOMContentLoaded', function () {
        new app.Team();
      });
    </script>
  </div>

</section>
