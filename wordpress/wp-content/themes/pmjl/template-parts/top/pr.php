<section class="pr gtm_depth" data-gtmev="[PAGE DEPTH] PR">
  <div class="wow fadeIn">
    <h2 class="pr__ttl">
      <img src="<?php echoAssets('img'); ?>/common/battlegrounds.png" alt="BATTLEGROUNDS">
    </h2>
    <h3 class="pr__subttl">PUBG MOBILEは<br class="sp">ココが面白い！</h3>
    <div class="pr__wrap">
      <p class="pr__text">
        『PUBG MOBILE』は最大100人のプレイヤーが、マップ内にある装備などを駆使して"最後の1人"になるまで生き抜くバトルロイヤルゲームです。<br>
        プレイヤーは段々狭くなる安全地帯内で、ランダムに配置されている武器や車両、装備アイテムを駆使して生存競争に臨みます。<br>
        最後の1人まで生き残ったプレイヤーの画面には「勝った!勝った!夕飯はドン勝だ!!」というメッセージが表示されるため、プレイヤーは「ドン勝」を目指して戦います。
      </p>
      <a href="https://pubgmobile.jp/" target="_blank" class="pr__link gtm_click" data-gtmev="[EXTERNAL LINK] PR - OFFICIAL WEB">
        OFFICIAL WEBへ
      </a>
    </div>
    <div class="pr__btn-wrap">
      <a href="https://apps.apple.com/jp/app/pubg-mobile/id1366526331" target="_blank" class="btn-app gtm_click" data-gtmev="[EXTERNAL LINK] PR - App Store">
        <img src="<?php echoAssets('img'); ?>/common/app.png" alt="App Store">
      </a>
      <a href="https://play.google.com/store/apps/details?id=com.pubg.krmobile&hl=ja" target="_blank"
         class="btn-google gtm_click" data-gtmev="[EXTERNAL LINK] PR - Google Play">
        <img src="<?php echoAssets('img'); ?>/common/google.png" alt="Google Play">
      </a>
    </div>
  </div>
</section>
