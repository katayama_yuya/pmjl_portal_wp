<section id="overview" class="open gtm_depth" data-gtmev="[PAGE DEPTH] OVERVIEW">
  <div class="wow fadeIn">
    <p class="open__copy">目指せ頂点、そして世界へ</p>
    <p class="open__body">2020年秋、日本最強チームが決まる<br>
      ここから世界へのサクセスストーリーが始まる</p>
    <h2 class="open__ttl">PUBG MOBILE<br>JAPAN LEAGUE SEASON 0<br>開幕へ</h2>
    <div class="open__subttl-wrap">
      <h3 class="open__subttl1">
        <picture>
          <source media="(max-width: 750px)"
                  srcset="<?php echoAssets('img'); ?>/top/open-subttl1-sp.png">
          <img src="<?php echoAssets('img'); ?>/top/open-subttl1.png" alt="大会賞金総額1000万円">
        </picture>
      </h3>
      <h3 class="open__subttl2">
        <picture>
          <source media="(max-width: 750px)"
                  srcset="<?php echoAssets('img'); ?>/top/open-subttl2-sp.png">
          <img src="<?php echoAssets('img'); ?>/top/open-subttl2.png" alt="優勝チーム400万円&世界大会出場権">
        </picture>
      </h3>
    </div>
  </div>
</section>
