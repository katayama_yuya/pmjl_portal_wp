<?php
$cast_pid = get_page_by_path('cast', 'OBJECT', 'editable_contents')->ID;
?>

<section class="cast gtm_depth" data-gtmev="[PAGE DEPTH] CAST">
  <div class="wow fadeIn">
    <div class="ttl">
      <h2 class="ttl__en ttl__en--mb">
        CAST
      </h2>
    </div>
    <div class="swiper-container cast__list">
      <div class="swiper-wrapper">

        <?php if(have_rows('casts', $cast_pid)): ?>
          <?php while(have_rows('casts', $cast_pid)): the_row(); ?>
          <div class="swiper-slide">
            <div class="cast__item">
              <div class="cast__img">
                <img src="<?php the_sub_field('pict'); ?>" alt="<?php the_sub_field('name'); ?>">
              </div>
              <div class="cast__text">
                <p class="cast__cate"><?php the_sub_field('title'); ?></p>
                <h3 class="cast__name"><?php the_sub_field('name'); ?></h3>
                <a class="cast__link" href="https://twitter.com/<?php the_sub_field('twitter'); ?>" target="_blank">@<?php the_sub_field('twitter'); ?></a>
                <p class="cast__body"><?php the_sub_field('text'); ?></p>
              </div>
            </div>
          </div>
          <?php endwhile; ?>
        <?php endif; ?>

      </div>
      <script>
        document.addEventListener('DOMContentLoaded', function () {
          new app.Cast();
        });
      </script>

    </div>
  </div>
</section>
