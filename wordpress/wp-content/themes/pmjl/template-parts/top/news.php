<?php
  $news_q = new WP_Query(
    array(
      'post_type'=>'news',
      'post_status' => 'publish',
      "posts_per_page" => 2
    )
  );
  $news_count = $news_q->post_count;
  if ( $news_count > 0 ):
?>
<section class="news-top gtm_depth" data-gtmev="[PAGE DEPTH] NEWS">
<div class="wow fadeIn">
  <div class="ttl ttl--short">
    <h2 class="ttl__en">
      NEWS
    </h2>
  </div>

  <section class="news-list">
    <ul class="news-list__list">
      <?php if ($news_q->have_posts()) : ?>
          <?php while ($news_q->have_posts()) : $news_q->the_post(); ?>
            <li class="news-list__item">
              <a class="news-list__link gtm_click" data-gtmev="[INTERNAL LINK] NEWS - Single <?php the_time('Y.m.d'); echo ' '; the_ID(); ?>" href="<?php the_permalink(); ?>">
                <span class="news-list__date"><?php the_time('Y.m.d'); ?></span>
                <span class="news-list__ttl"><?php the_title() ?></span>
              </a>
            </li>
          <?php endwhile; ?>
        <?php endif; ?>
    </ul>
  </section>

  <div class="news-top__link-wrap">
    <a class="news-top__link gtm_click" data-gtmev="[INTERNAL LINK] NEWS - MORE" href="news/">
      MORE<span></span>
    </a>
  </div>
</div>

</section>

<?php endif; ?>
