    <section class="list-link gtm_depth" data-gtmev="[PAGE DEPTH] LINKS">
      <div class="wow fadeIn">
        <div class="list-link__wrap">
          <a href="<?php echoAssets('download'); ?>/pmjls0_rulebook.pdf" class="list-link__left gtm_click" data-gtmev="[DL] LINKS- Rule Book" target="_blank">
            <span class="list-link__mt">
              <h2 class="list-link__ttl">ルールブック</h2>
              <p class="list-link__ttl-s">2020年8月11日 更新</p>
            </span>
          </a>
          <a href="<?php echo home_url(); ?>/entry/" class="list-link__center gtm_click" data-gtmev="[EXTERNAL LINK] LINKS - Player Entry">
            <span class="">
              <h2 class="list-link__ttl">プレイヤー<br class="sp">エントリー<br class="sp">サイト</h2>
            </span>
          </a>
          <a href="<?php echo home_url(); ?>/contact/" class="list-link__right gtm_click" data-gtmev="[INTERNAL LINK] LINKS - CONTACT">
            <span class="">
              <h2 class="list-link__ttl">お問い合わせは<br class="sp">こちら</h2>
            </span>
          </a>
        </div>
      </div>
    </section>
