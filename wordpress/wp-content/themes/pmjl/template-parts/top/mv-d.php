<?php
$transition = get_field('transition', $top_content_pid );
$c_archives = $transition['archive'];
$prompt = $transition['prompt'];
?>
<section class="head">
  <p class="head__logo">
    <picture>
      <source media="(max-width : 765px)" srcset="<?php echoAssets('img'); ?>/common/logo.svg">
      <img class="head__img" src="<?php echoAssets('img'); ?>/common/head-logo.png" alt="JAPAN LEAGUE PUBG MOBILE SEASON0">
    </picture>
  </p>
</section>

<section class="report">
  <div class="wow fadeIn">
    <h2 class="report-ttl">
      <span class="report-ttl__en"><?php echo $transition['catch_en']; ?></span>
      <span class="report-ttl__ja"><?php echo $transition['catch_jp']; ?></span>
    </h2>
    <section class="report-slide">
      <div class="report-slide__wrap">
        <div class="swiper-container report-slide__list">
          <div class="swiper-wrapper">
          <?php foreach($c_archives as $archive) :
            $yid = get_youtube_id($archive['youtube_link']);
            $ythumb = get_youtube_thumbnail($yid);
            ?>
            <div class="swiper-slide">
              <div class="report-slide__item gtm_click" data-gtmev="[WATCH YT] REPORT - YT <?php echo $archive['text']; ?>" data-youtube-id="<?php echo $yid; ?>">
                <div class="report-slide__img">
                    <img src="<?php echo $ythumb ?>" alt="<?php echo strip_tags($archive['text']); ?>">
                  <div class="report-slide__btn">
                    <img src="<?php echoAssets('img'); ?>/common/btn-yt.svg" alt="PLAY">
                  </div>
                </div>
                <h3 class="report-slide__ttl"><?php echo $archive['text']; ?></h3>
              </div>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
        <div class="report-slide__prev"></div>
        <div class="report-slide__next active"></div>
      </div>
    </section>
    <script>
      document.addEventListener('DOMContentLoaded', function () {
      new app.Report();
      });
    </script>
    <section class="report-body" id="reportBody">
      <h3 class="report-body__ttl"><?php echo $prompt['prompt_section']; ?></h3>
      <p class="report-body__copy"><?php echo $prompt['prompt_title']; ?></p>
      <p class="report-body__body">
        <?php echo $prompt['prompt_content']; ?><br>
      </p>
      <p class="report-body__body disable">
        <?php echo $prompt['prompt_content_next']; ?>
      </p>
      <div class="report-body__mask"></div>
    </section>
    <div class="report-more" id="reportMore">
      <div class="report-more__wrap gtm_click" data-gtmev="[CLICK] REPORT - Tap to read report news">
        <div class="report-more__img">
          <span></span>
          <span></span>
        </div>
        <p class="report-more__txt">タップして速報を見る</p>
      </div>
    </div>
  </section>

</section>
