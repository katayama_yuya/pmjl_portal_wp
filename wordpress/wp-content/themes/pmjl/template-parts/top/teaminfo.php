
  <h1 class="ttl">
    <span class="ttl__en">TEAM INFO</span>
    <span class="ttl__ja">出場チーム一覧</span>
  </h1>

<?php
  $info_q = new WP_Query(
    array(
      'post_type'=>'teaminfo',
      'post_status' => 'publish',
    )
  );
?>

<?php if ($info_q->have_posts()) : ?>
  <?php while ($info_q->have_posts()) : $info_q->the_post(); ?>

  <?php if(have_rows('team_info')): ?>
    <?php while(have_rows('team_info')): the_row(); ?>

      <div>
        <img src="<?php the_sub_field('team_img'); ?>" alt="<?php the_sub_field('team_name'); ?>">
      </div>
      <div>
        <img src="<?php the_sub_field('team_logo'); ?>" alt="<?php the_sub_field('team_name'); ?>">
        <?php the_sub_field('team_name'); ?>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>

  <?php endwhile; ?>
<?php endif; ?>

<a href="<?php echo home_url('/teaminfo'); ?>">チーム一覧ページ</a>