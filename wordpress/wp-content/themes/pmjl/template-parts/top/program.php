<section id="program" class="program gtm_depth" data-gtmev="[PAGE DEPTH] PROGRAM">
  <div class="wow fadeIn">
    <div class="ttl">
      <h2 class="ttl__en">
        PROGRAM
      </h2>
      <span class="ttl__ja">大会構成</span>
    </div>
    <div class="program__wrap wow">
      <div class="program__table">
        <ul class="flow">
          <li class="flow__item">
            <picture>
              <source media="(orientation: landscape) and (max-width : 765px)"
                      srcset="<?php echoAssets('img'); ?>/top/flow01-sp.svg">
              <img src="<?php echoAssets('img'); ?>/top/flow01.svg" alt="GROUP STAGE1">
            </picture>
          </li>
          <li class="flow__item">
            <picture>
              <source media="(orientation: landscape) and (max-width : 765px)"
                      srcset="<?php echoAssets('img'); ?>/top/flow02-sp.svg">
              <img src="<?php echoAssets('img'); ?>/top/flow02.svg" alt="GROUP STAGE2">
            </picture>
          </li>
          <li class="flow__item">
            <picture>
              <source media="(orientation: landscape) and (max-width : 765px)"
                      srcset="<?php echoAssets('img'); ?>/top/flow03-sp.svg">
              <img src="<?php echoAssets('img'); ?>/top/flow03.svg" alt="GROUP STAGE3">
            </picture>
          </li>
          <li class="flow__item">
            <picture>
              <source media="(orientation: landscape) and (max-width : 765px)"
                      srcset="<?php echoAssets('img'); ?>/top/flow04-sp.svg">
              <img src="<?php echoAssets('img'); ?>/top/flow04.svg" alt="SEMI FINAL">
            </picture>
          </li>
          <li class="flow__item">
            <picture>
              <source media="(orientation: landscape) and (max-width : 765px)"
                      srcset="<?php echoAssets('img'); ?>/top/flow05-sp.svg">
              <img src="<?php echoAssets('img'); ?>/top/flow05.svg" alt="GRAND FINAL">
            </picture>
          </li>
        </ul>
      </div>
      <div class="program__table program__table2">
        <div class="program__border">
          <h3 class="program__ttl">エントリー受付</h3>
          <p class="program__text">エントリー受付は終了しました</p>
        </div>
        <div class="program__border">
          <h3 class="program__ttl">大会期間</h3>
          <p class="program__text">2020年8月29日（土）～10月18日（日）</p>
        </div>
        <div class="program__border program__border--pb">
          <h3 class="program__ttl">大会ルール</h3>
          <p class="program__text program__text--pb0">
            総合順位（順位とキル数のポイント）を<br>
            評価します。<br>
            参加チーム数に制限はありません。
          </p>
          <p class="program__text program__text--pt">
            その他大会参加に関する詳細は、<br>
            ルールブックをご確認ください。
          </p>
          <a href="<?php echoAssets('download'); ?>/pmjls0_rulebook.pdf" target="_blank"
             class="btn-circle gtm_click" data-gtmev="[DL] PROGRAM - Rule Book">ルールブック（PDF）</a>
        </div>
      </div>
    </div>
  </div>
</section>
