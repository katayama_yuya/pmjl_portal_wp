<?php
// ヘッダーで ARCHIVE 有無チェックが必要なので、
// 変数定義は header_menu.php で行っている
if( $active_exist ):
?>
<section id="archives" class="archive gtm_depth" data-gtmev="[PAGE DEPTH] ARCHIVES">

  <div class="wow fadeIn">

    <h1 class="ttl">
      <span class="ttl__en">ARCHIVES</span>
      <span class="ttl__ja">大会アーカイブス</span>
    </h1>

    <nav id="archiveNav" class="stage-nav stage-nav--nallow">
      <div class="stage-ctl">
        <span class="btn-prev"><img class="img" src="<?php echoAssets('img'); ?>/common/stage-nav-arw-l.svg"></span>
        <span class="btn-next"><img class="img" src="<?php echoAssets('img'); ?>/common/stage-nav-arw-r.svg"></span>
      </div>
      <ul class="stage-nav__list">
        <li data-round="stage1"
          class="stage-nav__item<?php if(!$g['stage1']['active']): ?> disable<?php endif; if($g['stage1']['newest']): ?> active<?php endif; ?> gtm_click" data-gtmev="[CLICK] ARCHIVE Tab - <?php echo $stages_name['stage1']; ?>">
          GROUP<br>STAGE 1
        </li>
        <li data-round="stage2"
          class="stage-nav__item<?php if(!$g['stage2']['active']): ?> disable<?php endif; if($g['stage2']['newest']): ?> active<?php endif; ?> gtm_click" data-gtmev="[CLICK] ARCHIVE Tab - <?php echo $stages_name['stage2']; ?>">
          GROUP<br>STAGE 2
        </li>
        <li data-round="stage3"
          class="stage-nav__item<?php if(!$g['stage3']['active']): ?> disable<?php endif; if($g['stage3']['newest']): ?> active<?php endif; ?> gtm_click" data-gtmev="[CLICK] ARCHIVE Tab - <?php echo $stages_name['stage3']; ?>">
          GROUP<br>STAGE 3
        </li>
        <li data-round="semifinal"
          class="stage-nav__item<?php if(!$g['semifinal']['active']): ?> disable<?php endif; if($g['semifinal']['newest']): ?> active<?php endif; ?> gtm_click" data-gtmev="[CLICK] ARCHIVE Tab - <?php echo $stages_name['semifinal']; ?>">
          SEMI<br>FINAL
        </li>
        <li data-round="grandfinal"
          class="stage-nav__item<?php if(!$g['grandfinal']['active']): ?> disable<?php endif; if($g['grandfinal']['newest']): ?> active<?php endif; ?> gtm_click" data-gtmev="[CLICK] ARCHIVE Tab - <?php echo $stages_name['grandfinal']; ?>">
          GRAND<br>FINAL
        </li>
      </ul>
    </nav>

    <h2 class="archive-ttl">
      <span>配信アーカイブ</span>
    </h2>


    <div class="archive-slide-wrap">
      <?php
      $i = 1;
      foreach( $stages as $s ) :
        $g_fields = $g[$s];
        $bas = $g_fields['broadcast_archive'];
        $mas = $g_fields['match_archive'];
        ?>
        <section data-round="<?php echo $s ?>" class="archive-slide">
          <div class="archive-slide__wrap">
            <div class="swiper-container archive-slide__list archive-slide__list<?php echo $i; ?>">
              <div class="swiper-wrapper">

                <?php foreach($bas as $ba) :
                  $yid = get_youtube_id($ba['youtube_link']);
                  $thm = get_youtube_thumbnail($yid);
                  ?>
                  <div class="swiper-slide">
                    <div class="archive-slide__item gtm_click" data-gtmev="[WATCH YT] ARCHIVES - YT <?php echo $ba['text']; ?>" data-youtube-id="<?php echo $yid; ?>">
                      <div class="archive-slide__img">
                        <img src="<?php echo $thm ?>" alt="<?php echo $ba['text']; ?>">
                        <div class="archive-slide__btn">
                          <img src="<?php echoAssets('img'); ?>/common/btn-yt.svg" alt="PLAY">
                        </div>
                      </div>
                      <h3 class="archive-slide__ttl"><?php echo $ba['text']; ?></h3>
                    </div>
                  </div>
                <?php endforeach; ?>

              </div>
            </div>
            <div class="archive-slide__prev archive-slide__prev<?php echo $i; ?>"></div>
            <div class="archive-slide__next archive-slide__next<?php echo $i; ?>"></div>
          </div>
          <ul class="archive-link">
            <?php foreach($mas as $ma) :
              $yid = get_youtube_id($ma['youtube_link']);
              ?>
              <li class="archive-link__item gtm_click" data-gtmev="[WATCH YT] ARCHIVES - MATCH YT  <?php echo $ba['text']; ?>"  data-youtube-id="<?php echo $yid; ?>">
                <?php echo $ma['text']; ?>
              </li>
            <?php endforeach; ?>
          </ul>
        </section>
      <?php
        $i++;
        endforeach;
      ?>
    </div>

    <script>
      document.addEventListener('DOMContentLoaded', function() {
        new app.Archive();
      });
    </script>

  </div>
</section>
<?php endif; ?>
