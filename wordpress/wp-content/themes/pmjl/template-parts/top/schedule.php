<?php
$schedule_pid = get_page_by_path('schedule', 'OBJECT', 'editable_contents')->ID;
$group_stages = get_field('group_stage', $schedule_pid );
$finals = get_field('finals', $schedule_pid );
?>
<section id="schedule" class="schedule gtm_depth" data-gtmev="[PAGE DEPTH] SCHEDULE - GROUP STAGES">
  <div class="wow fadeIn">
    <div class="ttl">
      <h2 class="ttl__en">
        SCHEDULE
      </h2>
      <span class="ttl__ja">大会スケジュール</span>
    </div>
    <div class="schedule__wrap">

    <div class="schedule__table">
      <h3 class="schedule__subttl">GROUP STAGES</h3>
      <table class="wow fadeIn" data-wow-delay="300ms">
        <tr class="">
          <th class="schedule__tr1-1 schedule__en">STAGE</th>
          <th class="schedule__tr2-1 schedule__ja"></th>
          <th class="schedule__tr3-1 schedule__ja">日程</th>
          <th class="schedule__tr4-1 schedule__ja">視聴</th>
        </tr>
          <?php
            for( $i=1; $i<=3; $i++) :
              $rows = $group_stages['group_stage_'.$i];
              for( $j=1; $j<=count($rows); $j++) :
                 $row = $rows[$j-1];
                 $yid = get_youtube_id($row['youtube_link']);
                 $yttl = get_youtube_title($row['youtube_link']);
            ?>
              <tr class="">
                <?php if ( $j == 1 ): ?>
                <td class="schedule__td<?php echo $i; ?> schedule__body-l" rowspan="<?php echo count($rows); ?>">
                  GROUP<br>
                  STAGE <?php echo $i; ?><br>
                  <span class="schedule__body-s">オンライン開催</span>
                </td>
                <?php endif; ?>
                <td class="schedule__td2 schedule__body-m">
                  <?php echo $row['day']; ?>
                </td>
                <td class="schedule__td3 schedule__body-m">
                  <?php echo date('n/j', strtotime($row['date'])); ?>
                  <span class="schedule__body-s"><?php echo get_weekday_j( date('w', strtotime($row['date'])) ); ?></span>
                </td>
                <td class="schedule__td4">
                  <?php if ($row['youtube_link'] != ''): ?>
                  <a data-youtube-id="<?php echo $yid; ?>" href="#" class="btn-yt gtm_click" data-gtmev="[WATCH YT] SCHEDULE - YT GROUP STAGE <?php echo $i; ?> <?php echo $row['day']; ?>">
                  <?php else: ?>
                  <a data-youtube-id="" href="#" class="btn-yt disable">
                  <?php endif; ?>
                    <img src="<?php echoAssets('img'); ?>/common/btn-yt.svg" alt="youtube">
                    <img class="hover" src="<?php echoAssets('img'); ?>/common/btn-yt-hover.svg" alt="youtube">
                  </a>
                </td>
              </tr>
            <?php endfor; ?>
        <?php endfor; ?>
      </table>
    </div>

      <div class="schedule__table schedule__table--mr gtm_depth" data-gtmev="[PAGE DEPTH] SCHEDULE - FINALS">
        <h3 class="schedule__subttl">FINALS</h3>
        <table class="wow fadeIn" data-wow-delay="600ms">
          <tr class="">
            <th class="schedule__tr1-1 schedule__en">STAGE</th>
            <th class="schedule__tr2-1 schedule__ja"></th>
            <th class="schedule__tr3-1 schedule__ja">日程</th>
            <th class="schedule__tr4-1 schedule__ja">視聴</th>
          </tr>
          <?php
            $rows = $finals['semi_final'];
            for( $j=1; $j<=count($rows); $j++) :
               $row = $rows[$j-1];
               $yid = get_youtube_id($row['youtube_link']);
               $yttl = get_youtube_title($row['youtube_link']);
           ?>
              <tr class="">
                <?php if ( $j == 1 ): ?>
                <td class="schedule__td5 schedule__body-l" rowspan="<?php echo count($rows); ?>">
                  SEMI<br>
                  FINAL<br>
                  <span class="schedule__body-s">オンライン開催</span>
                </td>
                <?php endif; ?>
                <td class="schedule__td6 schedule__body-m">
                  <?php echo $row['day']; ?>
                </td>
                <td class="schedule__td7 schedule__body-m">
                  <?php echo date('n/j', strtotime($row['date'])); ?>
                  <span class="schedule__body-s"><?php echo get_weekday_j( date('w', strtotime($row['date'])) ); ?></span>
                </td>
                <td class="schedule__td8">
                  <?php if ($row['youtube_link'] != ''): ?>
                  <a data-youtube-id="<?php echo $yid; ?>" href="#" class="btn-yt gtm_click" data-gtmev="[WATCH YT] SCHEDULE - YT SEMI FINAL <?php echo $row['day']; ?>">
                  <?php else: ?>
                  <a data-youtube-id="" href="#" class="btn-yt disable">
                  <?php endif; ?>
                    <img src="<?php echoAssets('img'); ?>/common/btn-yt.svg" alt="youtube">
                    <img class="hover" src="<?php echoAssets('img'); ?>/common/btn-yt-hover.svg" alt="youtube">
                  </a>
                </td>
              </tr>
          <?php endfor; ?>

          <?php
            $rows = $finals['grand_final'];
            for( $j=1; $j<=count($rows); $j++) :
               $row = $rows[$j-1];
               $yid = get_youtube_id($row['youtube_link']);
               $yttl = get_youtube_title($row['youtube_link']);
           ?>
              <tr class="">
                <?php if ( $j == 1 ): ?>
                <td class="schedule__td5 schedule__td5--color2 schedule__body-l" rowspan="<?php echo count($rows); ?>">
                  GRAND<br>
                  FINAL<br>
                  <span class="schedule__body-s">オフライン開催</span>
                </td>
                <?php endif; ?>
                <td class="schedule__td6 schedule__body-m">
                  <?php echo $row['day']; ?>
                </td>
                <td class="schedule__td7 schedule__body-m">
                  <?php echo date('n/j', strtotime($row['date'])); ?>
                  <span class="schedule__body-s"><?php echo get_weekday_j( date('w', strtotime($row['date'])) ); ?></span>
                </td>
                <td class="schedule__td8">
                  <?php if ($row['youtube_link'] != ''): ?>
                  <a data-youtube-id="<?php echo $yid; ?>" href="#" class="btn-yt gtm_click" data-gtmev="[WATCH YT] SCHEDULE - YT GRAND FINAL <?php echo $row['day']; ?>">
                  <?php else: ?>
                  <a data-youtube-id="" href="#" class="btn-yt disable">
                  <?php endif; ?>
                    <img src="<?php echoAssets('img'); ?>/common/btn-yt.svg" alt="youtube">
                    <img class="hover" src="<?php echoAssets('img'); ?>/common/btn-yt-hover.svg" alt="youtube">
                  </a>
                </td>
              </tr>
          <?php endfor; ?>

        </table>
      </div>
    </div>
  </div>
</section>
