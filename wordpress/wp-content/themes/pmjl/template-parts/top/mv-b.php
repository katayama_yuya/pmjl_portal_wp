<?php
$on_day = get_field('on_day', $top_content_pid );
$yid = get_youtube_id($on_day['youtube_link']);
$ythumb = get_youtube_thumbnail($yid);

$time_schedules = $on_day['time_schedule'];
$point = $on_day['point'];

$ts_highlight_num = -1;
for ( $i = 0; $i < count($time_schedules) ; $i ++) {
  if( $time_schedules[$i]['ts_highlight'] ) {
    $ts_highlight_num = $i;
  }
}
?>


<section class="head">
  <p class="head__logo">
    <picture>
      <source media="(max-width : 765px)" srcset="<?php echoAssets('img'); ?>/common/logo.svg">
      <img class="head__img" src="<?php echoAssets('img'); ?>/common/head-logo.png" alt="JAPAN LEAGUE PUBG MOBILE SEASON0">
    </picture>
  </p>
</section>

<section class="live">
  <div class="wow fadeIn">

    <section class="live-head">

      <h2 class="live-head__ttl">
        <?php if($on_day['live_icon']): ?>
          <span><img src="<?php echoAssets('img'); ?>/live/icon.svg" alt="LIVE"></span>
        <?php endif; ?>
        <?php echo $on_day['catch_word']; ?>
      </h2>
      <div class="live-head__body" style="">
        <div class="live-head__body-bg" style="background-image: url(<?php echo $ythumb; ?>)"></div>

        <div class="live-head__img gtm_click" data-gtmev="[WATCH YT] LIVE - YT" data-youtube-id="<?php echo $yid ?>">
          <img src="<?php echo $ythumb; ?>" alt="<?php echo strip_tags($on_day['catch_word']); ?>">
          <div class="live-head__btn">
            <img src="<?php echoAssets('img'); ?>/common/btn-yt.svg" alt="PLAY">
          </div>

          <div class="live-head__typo live-head__typo--l"><img src="<?php echoAssets('img'); ?>/live/typo.svg" alt="PUBG MOBILE JAPAN LEAGUE SEASON 0">
          </div>
          <div class="live-head__typo live-head__typo--r"><img src="<?php echoAssets('img'); ?>/live/typo.svg" alt="PUBG MOBILE JAPAN LEAGUE SEASON 0">
          </div>

        </div>
      </div>
    </section>

    <section id="liveBody" class="live-body">
      <div class="live__bg">
        <img src="<?php echoAssets('img'); ?>/live/bg-logo.svg" alt="">
      </div>
      <div class="live-body__wrap">
        <h3 data-live-target="schedule" class="live-ttl live-ttl--schedule gtm_click" data-gtmev="[CLICK] LIVE Tab - TIME SCHEDULE">
          <span class="live-ttl__en"> TIME <br class="sp">SCHEDULE</span>
          <span class="live-ttl__ja"><?php echo $on_day['time_schedule_text']; ?></span>
        </h3>
        <div data-live-id="schedule" class="live-content ">
          <ul class="live-schedule">
            <?php for ( $i = 0; $i < count($time_schedules) ; $i ++): ?>
              <li class="live-schedule__item<?php if($i == $ts_highlight_num): ?> active<?php endif; ?>">
                <span><?php echo $time_schedules[$i]['ts_time']; ?></span>
                <?php echo $time_schedules[$i]['ts_notation']; ?>
              </li>
            <?php endfor; ?>
          </ul>
        </div>

        <h3 data-live-target="pointsview" class="live-ttl live-ttl--pointsview active gtm_click" data-gtmev="[CLICK] LIVE Tab - POINTS VIEW">
          <span class="live-ttl__en"> POINTS <br class="sp">VIEW</span>
          <span class="live-ttl__ja"><?php echo $on_day['points_view_text']; ?></span>
        </h3>
        <div data-live-id="pointsview" class="live-content active">
          <h4 class="live-subttl"><?php echo $point['point_section']; ?></h4>
          <p class="live-copy">
            <?php echo $point['point_title']; ?>
          </p>
          <p class="live-description">
            <?php echo $point['point_content']; ?>
          </p>
        </div>
      </div>
    </section>

  </div>
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      new app.Live();
    });
  </script>

</section>
