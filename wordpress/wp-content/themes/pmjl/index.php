<?php
get_header();
?>

<body <?php body_class(); ?>>
<div class="container">
  <?php
    include locate_template( 'template-parts/common/header_menu.php' );
    ?>

  <div class="content">
    <?php
      if ($top_content_mode == 'mode_a') {
          include locate_template('template-parts/top/mv-a.php');
      } elseif ($top_content_mode == 'mode_b') {
          include locate_template('template-parts/top/mv-b.php');
      } elseif ($top_content_mode == 'mode_b_final') {
          include locate_template('template-parts/top/mv-b_final.php');
      } elseif ($top_content_mode == 'mode_c') {
          include locate_template('template-parts/top/mv-c.php');
      } elseif ($top_content_mode == 'mode_c_final') {
          include locate_template('template-parts/top/mv-c_final.php');
      } elseif ($top_content_mode == 'mode_d') {
          include locate_template('template-parts/top/mv-d.php');
      }
      include locate_template( 'template-parts/top/news.php' );
      include locate_template( 'template-parts/top/open.php' );
      include locate_template( 'template-parts/top/program.php' );
      include locate_template( 'template-parts/top/schedule.php' );
      if ($held_finals_flg) {
        include locate_template('template-parts/top/teaminfo.php');
      }
      if ($held_flg) {
        include locate_template( 'template-parts/top/stats.php' );
        include locate_template( 'template-parts/top/archives.php' );
      }
      include locate_template( 'template-parts/top/cast.php' );
      include locate_template( 'template-parts/top/links.php' );
      include locate_template( 'template-parts/top/partner.php' );
      include locate_template( 'template-parts/top/pr.php' );
      ?>
  </div>

  <div id="overlay" class="overlay">
    <div id="overlyClose" class="overlay__close"></div>
    <div id="player" class="player"></div>
    <script>
      document.addEventListener('DOMContentLoaded', function () {
        new app.Youtube();
      });
    </script>
  </div>

  <?php
  get_footer();
  ?>

</div>
</body>
</html>
