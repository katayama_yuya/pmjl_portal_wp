<?php
get_header();
?>

<body <?php body_class(); ?>>
<div class="container">
  <?php
    include locate_template( 'template-parts/common/header_menu.php' );
    ?>

  <div class="content">

    <section class="head">
      <p class="head__logo">
        <picture>
          <source media="(max-width : 765px)" srcset="<?php echoAssets('img'); ?>/common/logo.svg">
          <img class="head__img" src="<?php echoAssets('img'); ?>/common/head-logo.png" alt="JAPAN LEAGUE PUBG MOBILE SEASON0">
        </picture>
      </p>
    </section>


  <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    <div class="news wow fadeIn">

      <h1 class="ttl">
        <span class="ttl__en">NEWS</span>
        <span class="ttl__ja">ニュース</span>
      </h1>


      <article class="news-item">

        <div class="news-item__head">
          <span class="news-item-date"><?php the_time('Y.m.d'); ?></span>
          <h1 class="news-item-ttl"><?php the_title() ?></h1>
        </div>

        <div>
          <?php the_content(); ?>
        </div>

      </article>

      <div class="news-nav">
        <div class="news-nav__wide">
          <?php
          if (get_previous_post()):
            previous_post_link('%link', '<span class="prev"></span>PREV');
          else:
          ?>
          <a class="news-nav__link disable" href="#"><span class="prev"></span>PREV</a>
          <?php
          endif;
          if (get_next_post()):
            next_post_link('%link', 'NEXT<span class="next"></span>');
          else:
          ?>
          <a class="news-nav__link disable" href="#">NEXT<span class="next"></span></a>
          <?php
          endif;
           ?>
        </div>
        <div class="news-nav__center">
          <a class="news-nav__top" href="/news/">NEWS INDEX</a>
        </div>

      </div>

    </div>
  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>
<?php endif; ?>

  </div>

  <?php
    get_footer();
  ?>

</div>
</body>
</html>
